var AvPoints = {
    labels: ["Kings of Wessex", "LEA avg", "England avg"],
    datasets: [
        {
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)'
            ],
            borderWidth: 1,
            data: [483.4, 315.4, 306.5]
        }
    ]
};

      //Bar Chart
var myBarChart = new Chart($("#exam-points"), {
    type: 'horizontalBar',
    data: AvPoints,
    options: {
      legend: {
        display: false
      },
      scales: {
          xAxes: [{
              display: true,
              ticks: {
                  beginAtZero: true,   // minimum value will be 0.
                  suggestedMax: 700
              }
          }]
      }
  }
});

      // Doughnut charts
      var ACEngMaths = {
    datasets: [
        {
            data: [68, 32],
            backgroundColor: [
                "green",
                "gray"
            ],
            hoverBackgroundColor: [
                "#FF6384",
                "gray"
            ]
        }]
};

      var ACAny = {
    datasets: [
        {
            data: [86, 14],
            backgroundColor: [
                "green",
                "gray"
            ],
            hoverBackgroundColor: [
                "#FF6384",
                "gray"
            ]
        }]
};

      var EBacc = {
    datasets: [
        {
            data: [35, 65],
            backgroundColor: [
                "green",
                "gray"
            ],
            hoverBackgroundColor: [
                "#FF6384",
                "gray"
            ]
        }]
};


// A* - C English Maths
var DoACEngMaths = new Chart($("#DoACEngMaths"), {
    type: 'doughnut',
    data: ACEngMaths
});

// A* - C
var DoACAny = new Chart($("#DoACAny"), {
    type: 'doughnut',
    data: ACAny
});

// A* - C English Maths
var DoEBacc = new Chart($("#DoEBacc"), {
    type: 'doughnut',
    data: EBacc
});




