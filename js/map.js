var map;
var nearSchools = '';
var school = '';


// when the whole document has loaded call the init function
$(document).ready(init);

function init () {
	console.log('Setting up the map');
	map = L.map('map').setView([52.48, -1.90], 10);

	// Attach an event to map moves
	map.on('moveend', mapMoved);

	//map.locate({setView: true});

	L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
	}).addTo(map);

	askForSchools();

}

var askForSchools = function () {
	// Get current bounding box
	var bounds ='bbox=' + map.getBounds().toBBoxString();
	console.log(bounds);
	// Make the JSON request
	$.get('https://www.cpdforteachers.com/schools', bounds, function (data) {
		nearSchools = data;
		renderMap(nearSchools);
	});
}

// Define custom icons
var outstandingIcon = L.icon({
    iconUrl: 'img/marker-icon-green.png',
    shadowUrl: 'img/marker-shadow.png',
	iconRetinaUrl: 'img/marker-icon-green-2x.png',
	iconSize:    [25, 41],
	iconAnchor:  [12, 41],
	popupAnchor: [1, -34],
	tooltipAnchor: [16, -28],
	shadowSize:  [41, 41]
});

var mapMoved = function () {
	console.log('Map has moved.  Time to get new data');
	askForSchools();
}

		

var renderMap = function (schools) {
	$.each(schools, function(){
	console.log(this.latitude);
	console.log(this.longitude);
	// Clear any older markers
	L.marker([this.latitude, this.longitude], {icon: outstandingIcon}).addTo(map)
    .bindPopup('<a href="#" onclick="goSchool(' + this.id + ')">' + this.name + '</a')
		});
	};


var mapPage = $('.map-page');
var schoolPage = $('.school-page');

schoolPage.hide();

var numberToRating = function (n) {	
	var s = '';
	var c = '';
	switch(n) {
    case '4':
        s = 'Inadequate';
        c = 'is-inadequate';
        f = 'fa fa-frown-o fa-4x is-inadequate';
        break;
    case '3':
        s = 'Requires Improvement';
        c = 'is-requires';
        f = 'fa fa-frown-o fa-4x is-requires';
        break;
    case '2':
    	s = 'Good';
    	c = 'is-good';
    	f = 'fa fa-smile-o fa-4x is-good';
    break;
    case '1':
    	s = 'Outstanding';
    	c = 'is-outstanding';
    	f = 'fa fa-smile-o fa-4x is-outstanding';
    break;
    default:
        s = 'No rating available'
	} 
	var rObject = {rText: s , rClass: c , rFa: f};
	return rObject;
}


var goSchool = function (n) {
	var school_id = n;
	school = _.findWhere(nearSchools, {id:school_id});
	console.log(school);
	// Dynamically update the text
	$('#school-name').text(school.name);
	$('#number-of-pupils').text(school.number_of_pupils);
	$('#gender').text(school.gender);
	$('#age-from').text(school.age_from);
	$('#age-to').text(school.age_to);
	$('#type').text(school.type);
	$('#address-1').text(school.address_1);
	$('#address-2').text(school.address_2);
	$('#post-code').text(school.postcode);
	$('#phone-number').text(school.phone);
	$('#website').text(school.website);

	var overallNum = school.ofsted_reports[0].overall;

	$('#overall-big').text(numberToRating(overallNum).rText)
		.removeClass()
		.addClass(numberToRating(overallNum).rClass);

	$('#overall-small').text(numberToRating(overallNum).rText)
		.removeClass()
		.addClass(numberToRating(overallNum).rClass);

	$('#report-date').text(numberToRating(school.ofsted_reports[0].publication_date).rText);

	$('#leadership').text(numberToRating(school.ofsted_reports[0].leadership).rText)
		.removeClass()
		.addClass(numberToRating(school.ofsted_reports[0].leadership).rClass);

	$('#behaviour').text(numberToRating(school.ofsted_reports[0].behaviour).rText)
		.removeClass()
		.addClass(numberToRating(school.ofsted_reports[0].behaviour).rClass);

	$('#teaching').text(numberToRating(school.ofsted_reports[0].teaching).rText)
		.removeClass()
		.addClass(numberToRating(school.ofsted_reports[0].teaching).rClass);

	$('#achievement').text(numberToRating(school.ofsted_reports[0].outcomes).rText)
		.removeClass()
		.addClass(numberToRating(school.ofsted_reports[0].outcomes).rClass);

	$('#sixth-form').text(numberToRating(school.ofsted_reports[0].sixth_form).rText)
		.removeClass()
		.addClass(numberToRating(school.ofsted_reports[0].sixth_form).rClass);

	$('#fa-leadership').removeClass().addClass(numberToRating(school.ofsted_reports[0].leadership).rFa);
	$('#fa-behaviour').removeClass().addClass(numberToRating(school.ofsted_reports[0].behaviour).rFa);
	$('#fa-teaching').removeClass().addClass(numberToRating(school.ofsted_reports[0].teaching).rFa);
	$('#fa-achievement').removeClass().addClass(numberToRating(school.ofsted_reports[0].outcomes).rFa);
	$('#fa-sixth-form').removeClass().addClass(numberToRating(school.ofsted_reports[0].sixth_form).rFa);



	// Pump a value into the gauge
	var ratingGauge = gauge('#rating-gauge', {
		size: 200,
		clipWidth: 200,
		clipHeight: 125,
		ringWidth: 30,
		maxValue: 8,
		transitionMs: 4000,
	});
	ratingGauge.render();
	ratingGauge.update(2 * (5 - overallNum) - 1);

	mapPage.hide();
	schoolPage.show();
}

var goMap = function () {
	//clear the gauge
	d3.select("svg").remove();

	mapPage.show();
	schoolPage.hide();
}



/*******************************
Gauge for overall Ofsted Rating
********************************/

var gauge = function(container, configuration) {
	var that = {};
	var config = {
		size						: 200,
		clipWidth					: 200,
		clipHeight					: 110,
		ringInset					: 20,
		ringWidth					: 20,
		
		pointerWidth				: 10,
		pointerTailLength			: 5,
		pointerHeadLengthPercent	: 0.9,
		
		minValue					: 0,
		maxValue					: 10,
		
		minAngle					: -90,
		maxAngle					: 90,
		
		transitionMs				: 750,
		
		majorTicks					: 4,
		labelFormat					: d3.format(',g'),
		labelInset					: 10,
		
		arcColorFn					: d3.interpolateHsl(d3.rgb('orange'), d3.rgb('green'))
	};
	var range = undefined;
	var r = undefined;
	var pointerHeadLength = undefined;
	var value = 0;
	
	var svg = undefined;
	var arc = undefined;
	var scale = undefined;
	var ticks = undefined;
	var tickData = undefined;
	var pointer = undefined;

	var donut = d3.layout.pie();
	
	function deg2rad(deg) {
		return deg * Math.PI / 180;
	}
	
	function newAngle(d) {
		var ratio = scale(d);
		var newAngle = config.minAngle + (ratio * range);
		return newAngle;
	}
	
	function configure(configuration) {
		var prop = undefined;
		for ( prop in configuration ) {
			config[prop] = configuration[prop];
		}
		
		range = config.maxAngle - config.minAngle;
		r = config.size / 2;
		pointerHeadLength = Math.round(r * config.pointerHeadLengthPercent);

		// a linear scale that maps domain values to a percent from 0..1
		scale = d3.scale.linear()
			.range([0,1])
			.domain([config.minValue, config.maxValue]);
			
		ticks = scale.ticks(config.majorTicks);
		tickData = d3.range(config.majorTicks).map(function() {return 1/config.majorTicks;});
		
		arc = d3.svg.arc()
			.innerRadius(r - config.ringWidth - config.ringInset)
			.outerRadius(r - config.ringInset)
			.startAngle(function(d, i) {
				var ratio = d * i;
				return deg2rad(config.minAngle + (ratio * range));
			})
			.endAngle(function(d, i) {
				var ratio = d * (i+1);
				return deg2rad(config.minAngle + (ratio * range));
			});
	}
	that.configure = configure;
	
	function centerTranslation() {
		return 'translate('+r +','+ r +')';
	}
	
	function isRendered() {
		return (svg !== undefined);
	}
	that.isRendered = isRendered;
	
	function render(newValue) {
		svg = d3.select(container)
			.append('svg:svg')
				.attr('class', 'gauge')
				.attr('width', config.clipWidth)
				.attr('height', config.clipHeight);
		
		var centerTx = centerTranslation();
		
		var arcs = svg.append('g')
				.attr('class', 'arc')
				.attr('transform', centerTx);
		
		arcs.selectAll('path')
				.data(tickData)
			.enter().append('path')
				.attr('fill', function(d, i) {
					return config.arcColorFn(d * i);
				})
				.attr('d', arc);
		
		var lg = svg.append('g')
				.attr('class', 'label')
				.attr('transform', centerTx);
		lg.selectAll('text')
				.data(ticks)
			.enter().append('text')
				.attr('transform', function(d) {
					var ratio = scale(d);
					var newAngle = config.minAngle + (ratio * range);
					return 'rotate(' +newAngle +') translate(0,' +(config.labelInset - r) +')';
				})
				.text(config.labelFormat);

		var lineData = [ [config.pointerWidth / 2, 0], 
						[0, -pointerHeadLength],
						[-(config.pointerWidth / 2), 0],
						[0, config.pointerTailLength],
						[config.pointerWidth / 2, 0] ];
		var pointerLine = d3.svg.line().interpolate('monotone');
		var pg = svg.append('g').data([lineData])
				.attr('class', 'pointer')
				.attr('transform', centerTx);
				
		pointer = pg.append('path')
			.attr('d', pointerLine/*function(d) { return pointerLine(d) +'Z';}*/ )
			.attr('transform', 'rotate(' +config.minAngle +')');
			
		update(newValue === undefined ? 0 : newValue);
	}
	that.render = render;
	
	function update(newValue, newConfiguration) {
		if ( newConfiguration  !== undefined) {
			configure(newConfiguration);
		}
		var ratio = scale(newValue);
		var newAngle = config.minAngle + (ratio * range);
		pointer.transition()
			.duration(config.transitionMs)
			.ease('elastic')
			.attr('transform', 'rotate(' +newAngle +')');
	}
	that.update = update;

	configure(configuration);
	
	return that;
};









